#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <conio.h>
#include <string.h>

typedef struct {
    int nr;             //The registration number, because upon enrollment of the child in the school he receives a registration number
    char FirstName[30];      //First Name 
    char LastName[30];		//Last Name
    char CNP[14];		// string ASCIIZ CNP
    int birthdate;              //birth year
    unsigned char examGrades[4];           //Exam scores for each child
    char si;
} ELEV;


void main()
{
    char numefd[30] = "..\\Elev_r_f.dat";
    FILE* f;
    ELEV x;
    int vb, j;
    char snr[30];
    char new_LastName[30]; 

    //update Last Name for CNP X
    fopen_s(&f, numefd, "rb+");
    if (!f)
        printf_s("\nYou have probably misplaced your file. Stopping here.");
    else
    {
        printf_s("\nSearch by CNP: "); //The criteria we are looking for
        gets_s(snr);
        while (strcmp(snr, "0") != 0)          //if input is "0" then stop
        {
            rewind(f);
            vb = 0;
            fread(&x, sizeof(ELEV), 1, f);
            while ((!feof(f)) && (!vb)) 
            {
                if (strcmp(x.CNP,snr) == 0)
                {
                    vb = 1;   //found
                    //visualize
                    printf_s("\nFirst Name : %s, CNP: %s, Last Name: %s, Birth Year: %d\n", x.FirstName, x.CNP, x.LastName, x.birthdate);
                    //update
                    //Convention: update the Last Name for CNP X
                    printf_s("\nNew last name value: ");
                    gets_s(new_LastName);      //read new value in separate variable
                    if (new_LastName != 0)                //confirm by entering a non zero new grade. zero means no change
                    { //change the record
                        strcpy(x.LastName, new_LastName);
                        //overwrite the old record in the file
                        fseek(f, ftell(f) - sizeof(ELEV), 0);
                        fwrite(&x, sizeof(ELEV), 1, f);
                        fseek(f, 0, 1);
                    }
                }
                fread(&x, sizeof(ELEV), 1, f);
            }
            if (!vb)
                printf_s("\nNo such ELEV.");
            printf_s("\nTry again? Search CNP: ");
            //if (strcmp(snr, "0") != 0)
              //  gets_s(snr);
            gets_s(snr);
        }
        fclose(f);
    }

    printf_s("\nDone. Press a key");
    _getch();
}