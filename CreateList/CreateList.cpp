#include <stdio.h>
#include <conio.h>
#include <string.h>

typedef struct {
	int nr;             //The registration number, because upon enrollment of the child in the school he receives a registration number
	char FirstName[30];      //First Name 
	char LastName[30];		//Last Name
	char CNP[14];		// string ASCIIZ CNP
	int birthdate;              //birth year
	unsigned char examGrades[4];           //Exam scores for each child
	char si;
} ELEV;

void main()
{
	char numefr[30] = ".\\Elev_r_f.dat";
	char numeft[30] = ".\\List.txt";
	FILE* f, * g;
	ELEV x;
	int crt, j;

	fopen_s(&f, numefr, "rb");
	if (!f)
		printf_s("\nYou have probably misplaced your file. Stopping here.");
	else
	{
		fopen_s(&g, numeft, "wt");
		fprintf_s(g, "%4s %2s %-10s %-10s %-7s %-10s %10s %10s %10s %10s", "Crt.", "Elev number", "First Name", "Last Name", "CNP", "Year", "   Romanian   ", "  Math   ", "   Informatics   ", "  English");
		crt = 0;
		fread(&x, sizeof(ELEV), 1, f);
		while (!feof(f))
		{
			if (x.si == 1)
			{
				fprintf_s(g, "\n%2d %6d %15s %10s %8s %8d", ++crt, x.nr, x.FirstName, x.LastName, x.CNP, x.birthdate);
				for (j = 0; j < 4; j++)
					fprintf_s(g, "%15d ", x.examGrades[j]);
			}
			fread(&x, sizeof(ELEV), 1, f);
		}

		fclose(g);
		fclose(f);
		printf_s("\nThe full list is in <%s>.", numeft);
	}

	printf_s("\nDone. Press a key", numeft);
	_getch();
}