#include <stdio.h>
#include <conio.h>

typedef struct {
    int nr;             //The registration number, because upon enrollment of the child in the school he receives a registration number
    char FirstName[30];      //First Name 
    char LastName[30];		//Last Name
    char CNP[14];		// string ASCIIZ CNP
    int birthdate;              //birth year
    unsigned char examGrades[4];           //Exam scores for each child
    char si;
} ELEV;

void main()
{
    char numefd[30] = "..\\Elev_r_f.dat";
    FILE* f;
    ELEV x;
    int vb, j;
    int snr;
    int new_grade;

    //update Romanian grade for Elev Number X
    fopen_s(&f, numefd, "rb+");
    if (!f)
        printf_s("\nYou have probably misplaced your file. Stopping here.");
    else
    {
        printf_s("\nSearch elev's number: "); //The criteria we are looking for
        scanf_s("%d", &snr);
        while (snr != 0)                         //if input is 0 then stop
        {
            rewind(f);
            vb = 0;
            fread(&x, sizeof(ELEV), 1, f);
            while ((!feof(f)) && (!vb))
            {
                if (x.nr == snr)
                {
                    vb = 1;   //found
                    //visualize
                    printf_s("\nFirst Name : %s, Last Name: %s, CNP: %d, Romanian grade: %d\n", x.FirstName, x.LastName, x.CNP, x.examGrades[0]);
                    //update
                    //Convention: update the Romanian grade for Elev Number X
                    printf_s("\nNew Romanian grade: ");
                    scanf_s("%d", &new_grade);       //read new value in separate variable
                    if (new_grade != 0)                //confirm by entering a non zero new grade. zero means no change
                    { //change the record
                        x.examGrades[0] = new_grade;
                        //overwrite the old record in the file
                        fseek(f, ftell(f) - sizeof(ELEV), 0);
                        fwrite(&x, sizeof(ELEV), 1, f);
                        fseek(f, 0, 1);
                    }
                }
                fread(&x, sizeof(ELEV), 1, f);
            }
            if (!vb)
                printf_s("\nNo such ELEV.");
            printf_s("\nTry again? Search elev nr.: ");
            scanf_s("%d", &snr);
        }
        fclose(f);
    }

    printf_s("\nDone. Press a key");
    _getch();
}