#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <conio.h>

typedef struct {
	int nr;             //The registration number, because upon enrollment of the child in the school he receives a registration number
	char FirstName[30];       //First Name 
	char LastName[30];		//Last Name
	char CNP[14];        //string ASCIIZ CNP
	int birthdate;              //birth year
	unsigned char examGrades[4];           //Exam scores for each child
	char si;
} ELEV;


int filesize(FILE* f, int rec_size)
{
	long crt_pos;
	int size;

	crt_pos = ftell(f);
	fseek(f, 0, SEEK_END);
	size = ftell(f) / rec_size;
	fseek(f, crt_pos, SEEK_SET);
	return size;
}

void main()
{
	char namefr[30] = "..\\Elev_r_f.dat";
	FILE* f;
	ELEV x;
	int i, key, dim;

	fopen_s(&f, namefr, "wb+");

	printf("Elev number: ");
	scanf_s("%d", &key);
	while (!feof(stdin))
	{
		//check key range
		dim = filesize(f, sizeof(ELEV));
		if (key >= dim)
		{ //extend the file
			x.si = 0;
			fseek(f, 0, SEEK_END);
			for (i = 0; i < key - dim; i++)
				fwrite(&x, sizeof(ELEV), 1, f);
		}
		//check if available position
		fseek(f, key * sizeof(ELEV), SEEK_SET);
		fread(&x, sizeof(ELEV), 1, f);
		if (x.si == 1)
			printf("\nError: duplicate key %d. Skipping.", key);
		else
		{
			x.nr = key;
			printf_s("First Name: ");
			gets_s(x.FirstName);
			gets_s(x.FirstName);
			printf_s("Last Name: ");
			gets_s(x.LastName);
			printf_s("CNP: ");
			gets_s(x.CNP);
			printf_s("Birth Date: ");
			scanf_s("%d", &x.birthdate);
			for (i = 0; i < 4; i++)
			{
				switch (i) {
				case 0:
					printf_s("Romanian: ");
					break;
				case 1:
					printf_s("Math: ");
					break;
				case 2:
					printf_s("Informatics: ");
					break;
				case 3:
					printf_s("English: ");
					break;
				}
				scanf_s("%d", &x.examGrades[i]);
			}
			x.si = 1;
			fseek(f, key * sizeof(ELEV), SEEK_SET);
			fwrite(&x, sizeof(ELEV), 1, f);
		}
		printf_s("Elev number (or Ctrl+Z x3): ");
		scanf_s("%d", &key);
	}

	fclose(f);
	fclose(stdin);

	printf("\nDone. File <%s> was created. Press a key.", namefr);
	_getch();
}