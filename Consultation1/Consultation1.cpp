#include <stdio.h>
#include <conio.h>


typedef struct {
    int nr;             //The registration number, because upon enrollment of the child in the school he receives a registration number
    char FirstName[30];       //First Name 
    char LastName[30];		//Last Name
    char CNP[14];        //string ASCIIZ CNP
    int birthdate;              //birth year
    unsigned char examGrades[4];           //Exam scores for each child
    char si;
} ELEV;


void main()
{
    char numefd[30] = "..\\ELEV_r_f.dat";
    FILE* f;
    ELEV x;
    int vb, j;
    int snr;

    //Consultation after release year
    fopen_s(&f, numefd, "rb+");
    if (!f)
        printf_s("\nYou have probably misplaced your file. Stopping here.");
    else
    {
        printf_s("\nSearch by birth year: "); //The elements' common element (Birth Year, First Name, CNP etc..)
        scanf_s("%d", &snr);
        while (snr != 0)                         //If is 0 then stop
        {
            rewind(f);
            vb = 0;
            fread(&x, sizeof(ELEV), 1, f);
            while ((!feof(f)))          //Searching for multiple records
            {
                if (x.birthdate == snr)
                {
                    vb = 1;   //found
                    //visualize
                    printf_s("\nFirst Name: %s, Last Name: %s, Birth Year: %d, CNP: %s, Romanian: %d, Math: %d, Informatics: %d, English: %d\n", x.FirstName, x.LastName, x.birthdate, x.CNP, x.examGrades[0], x.examGrades[1], x.examGrades[2], x.examGrades[3]);
                }
                fread(&x, sizeof(ELEV), 1, f);
            }
            if (!vb)
                printf_s("\nNo such ELEV.");
            printf_s("\nTry again? Search by another birth year: ");
            scanf_s("%d", &snr);
        }
        fclose(f);
    }

    printf_s("\nDone. Press a key");
    _getch();
}