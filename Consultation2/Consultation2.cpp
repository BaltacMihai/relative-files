#include <stdio.h>
#include <conio.h>
#include <string.h>


typedef struct {
    int nr;             //The registration number, because upon enrollment of the child in the school he receives a registration number
    char FirstName[30];       //First Name 
    char LastName[30];		//Last Name
    char CNP[14];        //string ASCIIZ CNP
    int birthdate;              //birth year
    unsigned char examGrades[4];           //Exam scores for each child
    char si;
} ELEV;



void main()
{
    char numefd[30] = "..\\ELEV_r_f.dat";
    FILE* f;
    ELEV x;
    int vb, j;
    char snr[30];

    //Consultation after FirstName Name
    fopen_s(&f, numefd, "rb+");
    if (!f)
        printf_s("\nYou have probably misplaced your file. Stopping here.");
    else
    {
        printf_s("\nSearch by First Name : "); //The elements' common element (Birth Year, First Name, CNP etc..)
        gets_s(snr);
        while (strcmp(snr, "0") != 0)          
        {
            rewind(f);
            vb = 0;
            fread(&x, sizeof(ELEV), 1, f);
            while ((!feof(f)))          //Searching for multiple records
            {
                if (strcmp(x.FirstName, snr) == 0)
                {
                    vb = 1;   //found at least one element
                    //visualize
                    printf_s("\nFirst Name: %s, Last Name: %s, Birth Year: %d, CNP: %s, Romanian: %d, Math: %d, Informatics: %d, English: %d\n", x.FirstName, x.LastName, x.birthdate, x.CNP, x.examGrades[0], x.examGrades[1], x.examGrades[2], x.examGrades[3]);
                }
                fread(&x, sizeof(ELEV), 1, f);
            }
            if (!vb)
                printf_s("\nNo such ELEV.");
            printf_s("\nTry again? Search by another First name: ");
            //if (strcmp(snr, "0") != 0)
            //    gets_s(snr);
            gets_s(snr);
        }
        fclose(f);
    }

    printf_s("\nDone. Press a key");
    _getch();
}